//
//  ViewController.swift
//  Palindrome Checker
//
//  Created by VJ Pranay on 19/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBAction func checkPalindrome(_ sender: Any) {
        
        guard let userInputText = inputTextFeild.text else {
            return
        }
        
        let lowserCaseString = userInputText.lowercased()
        
        let whiteSpaceRemoved = lowserCaseString.replacingOccurrences(of: " ", with: "")
        
        let punctuationRemovedString = whiteSpaceRemoved.components(separatedBy: CharacterSet.punctuationCharacters).joined()

        
        let reversedText = punctuationRemovedString.reversed()
        
  
        let result = isPalindrome(userString: punctuationRemovedString)
        
        let realStringText = "Your Input is \(userInputText)"
        
        realString.text = realStringText
        
        // let reversedStringText = "Reverse of it is \(reversedText)"
        
        reversedString.text = "Reverse of it is " + String(reversedText)
        
        if result {
            displayText.text = "\(userInputText) is a Palindrome"
        }
        else{
            displayText.text = "\(userInputText) is not a Palindrome"
        }
        
    }
    @IBOutlet weak var realString: UILabel!
    @IBOutlet weak var reversedString: UILabel!
    @IBOutlet weak var inputTextFeild: UITextField!
    @IBOutlet weak var displayText: UILabel!

}

