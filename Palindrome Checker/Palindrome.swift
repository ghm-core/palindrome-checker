//
//  Palindrome.swift
//  Palindrome Checker
//
//  Created by VJ Pranay on 19/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import Foundation
func isPalindrome(userString: String) -> Bool {
    let reversed = String(userString.reversed())
    if( userString == reversed ){
        return true
    }
    else{
        return false
    }
}
